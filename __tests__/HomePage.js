import 'react-native';
import React from 'react';
import HomePage from 'HomePage';
// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';
describe("snapshot tests", () => {
    it('snapshot renders correctly', () => {
        const tree = renderer.create(
          <HomePage
           paragraph1="welcome"
           paragraph2="instructions"
           paragraph3="more detail here"
           />
        ).toJSON();
      expect(tree).toMatchSnapshot();
      });
})