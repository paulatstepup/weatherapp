/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {
    Component
} from 'react';
import {
    AppRegistry
} from 'react-native';

import HomePage from 'HomePage';

export default class weatherwithyou extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return ( 
        <HomePage 
            paragraph1 = "Welcome to React Native Handbook"
            paragraph2 = "To get started, edit index.ios.js"
            paragraph3 = "Press Cmd+R to reload, Cmd+D or shake for dev menu" 
        />
        );
    }
}

AppRegistry.registerComponent('weatherwithyou', () => weatherwithyou);