watchman watch-del-all
rm -rf node_modules && npm install
rm -rf $TMPDIR/react-*
find ./node_modules -type d -name fbjs -and -not -path ./node_modules/fbjs -print -exec rm -rf "{}" \;
npm cache clean
npm start -- --reset-cache