/**
 * @providesModule HomePage
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';

export default class HomePage extends Component {
    constructor(props) {
        super(props);
    }
    render() {
      return (
        <View style={styles.container}>
            <Text style={styles.welcome}>
            {this.props.paragraph1}
            </Text>
            <Text style={styles.instructions}>
            {this.props.paragraph2}
            </Text>
            <Text style={styles.instructions}>
            {this.props.paragraph3}
            </Text>
      </View>
      );
    }
  }
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    instructions: {
      textAlign: 'center',
      color: '#333333',
      marginBottom: 5,
    },
  });