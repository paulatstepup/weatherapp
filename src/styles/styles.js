import { StyleSheet } from 'react-native';
const styles = StyleSheet.create({
    activityIndicator: {
        flex: 1,
        paddingTop: 20
    },
    basic: {
        flex: 1,
        paddingTop: 50,
        alignItems: 'center',
        justifyContent: 'flex-start'
    }
});

module.exports = { styles: styles };
