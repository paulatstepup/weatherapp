/**
 * @providesModule TemperatureBox
 */

import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View
} from 'react-native';

export default class TemperatureBox extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        if (this.props.feelsLike === true) {
            return (
                <View style={styles.altContainer}>
                    <Text style={styles.smallTitle}>feels like</Text>
                <View>
                    <Text style={styles.medDegree}>
                        {this.props.degrees}
                        <Text style={styles.symbol}>&nbsp;°
                            </Text>
                    </Text>
                </View>
                </View>
            );
        } else {
            return (
                <View style={styles.container}>
                    <Text style={styles.bigDegree}>
                        {this.props.degrees}
                        <Text style={styles.symbol}>&nbsp;°
                            </Text>
                    </Text>
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 0,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        height: 80,
        width: 80,
        borderWidth: 1.0,
        borderColor: 'black'
    },
    altContainer: {
        flex: 0,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        height: 80,
        width: 80,
        borderWidth: 1.0,
        borderColor: 'black'
    },
    bigDegree: {
        fontSize: 40,
        fontWeight: 'bold',
        lineHeight: 40
    },
    symbol: {
        fontSize: 28,
        lineHeight: 44,
        textAlignVertical: 'top'
    },
    smallTitle: {
        fontSize: 12
    },
    medDegree: {
        fontSize: 28,
        fontWeight: 'bold',
        lineHeight: 28
    },
});