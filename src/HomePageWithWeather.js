/**
 * @providesModule HomePageWithWeather
 */

import React, { Component } from 'react';
import Dimensions from 'Dimensions';
import TemperatureBox from 'TemperatureBox';
const { height, width } = Dimensions.get('window');
const containerHeight = height - 120;
console.log(`height ${height} width ${width}`);
import {
    StyleSheet,
    Text,
    View
} from 'react-native';

export default class HomePageWithWeather extends Component {
    constructor(props) {
        super(props);
        this.state = {
            location: "London",
            datetime: "04/07/17 14:36",
            lastRefreshedAt: "5 mins ago ...",
            temperature: 16,
            feelsLikeTemperature: 14,
            nextTemperature: 20,
            previousTemperature: 17,
            precipProbability: '25% ffs',
            windSpeed: 12,
            windBearing: 217,
            summary: "Partly Cloudy",
            cloudCover: 0.12,
            uvIndex: 1,
            visibility: 200,
            humidity: 0.56
        }
    }
    render() {
        return (
            <View style={styles.master}>
                <View style={styles.container}>
                    <View style={styles.tempContainer}>
                        <Text>London</Text>
                        <Text>04/07/17 14:36</Text>
                        <Text>5 mins ago ...</Text>
                    </View>
                    <View style={styles.tempContainerAlt}>
                        <TemperatureBox degrees="16"/>
                        <TemperatureBox feelsLike={true} degrees="14"/>
                    </View>
                    <View style={styles.tempContainer}>
                        <Text>3</Text>
                    </View>
                    <View style={styles.tempContainerAlt}>
                        <Text>4</Text>
                    </View>
                    <View style={styles.tempContainer}>
                        <Text>5</Text>
                    </View>
                </View>
                <View style={styles.bottomPanel}>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    master: {
        flex: 1,
        height: height
    },
    container: {
        flex: 0,
        top: 20,
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        height: containerHeight,
        width: width
    },
    bottomPanel: {
        flex: 1,
        backgroundColor: 'pink'
    },
    tempContainer: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        backgroundColor: 'white',
        width: width,
        justifyContent: 'space-around'
    },
    tempContainerAlt: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'white',
        justifyContent: 'space-around',
        width: 200
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});