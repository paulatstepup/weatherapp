/**
 * @providesModule Weather
 */
import React, { Component } from 'react';
import { ActivityIndicator, ListView, Text, View } from 'react-native';
import Geocoder from 'react-native-geocoder';
import moment from 'moment';
import { styles } from './styles/styles';

export default class Weather extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            latitude: null,
            longitude: null
        }
    }

    componentDidMount() {
        return fetch('https://s3-eu-west-1.amazonaws.com/stepupsoftwaremocks/darkskies.json')
            .then((response) => response.json())
            .then((responseJson) => {
                let LOC = {
                    lat: responseJson.latitude,
                    lng: responseJson.longitude
                };
                let dt = moment.unix(responseJson.currently.time).format("ddd do MMM YYYY");
                Geocoder.geocodePosition(LOC).then(res => {
                    // res is an Array of geocoding object (see below)
                    let loc = res[0].subLocality + ", " + res[0].locality;
                    this.setState({
                        isLoading: false,
                        latitude: responseJson.latitude,
                        longitude: responseJson.longitude,
                        dt: dt,
                        location: loc
                    }, function () {
                        // do something with new state
                        console.log(`lat:  ${this.state.latitude}`);
                        console.log(`long: ${this.state.longitude}`);
                    });
                })
                .catch(err => console.log(err));
            })
            .catch((error) => {
                console.error(error);
            });
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={styles.activityIndicator}>
                    <ActivityIndicator />
                </View>
            );
        }

        return (
            <View style={styles.basic}>
                <Text>{this.state.location}</Text>
                <Text>{this.state.dt}</Text>
            </View>
        );
    }
}